---
title: "Vítejte na Dni otevřených dveří Pátku!"
date: 2021-02-01T00:00:00+02:00
authors: [ "Sijisu" ]
---
Pokud se chcete dozvědět více o vědeckotechnickém kroužku Pátek, dorazili jste na správné místo.

## Video

Jestli jste **ještě neviděli video**, ve kterém Pátek představujeme, honem to napravte:

<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/x4fwjv9Timo' frameborder='0' allowfullscreen></iframe></div>

## Setkání s Pátečníky

Zajímá Vás něco konkrétního? Chcete zeptat, jak to na Pátku vypadá? Nebo se jen s Pátečníky pokecat? Tak se v Den otevřených dveří (2. 2. 2021) připojte **mezi 14. a 16. hodinou** přes [tento odkaz](https://meet.jit.si/Pátek).

### Nemůžete na setkání dorazit?

**Dorazte přímo na Online Pátek!** Každý pátek od 15:00 do cca 18:00 na [stejném odkazu](https://meet.jit.si/Pátek)!

## Přednášky

Přednášky pátečníků i externistů **najdete v [sekci Talks](https://patek.cz/talks/)** tady na stránkách. Zaujmout vás můžou třeba [Základy webového vývoje](https://www.youtube.com/watch?v=wo10Nogfvdg) nebo [Úvod do teorie her](https://patek.cz/talks/game-theory/).

## Stratocaching 2020 na Maker Faire Prague Online

V našem videu se toho bohužel více ke stratocachingu nevešlo, nicméně pokud vás tento projekt *[Žádné vědy](https://zadnaveda.cz/)*, na kterém jsme se i my malým dílem podíleli, zaujal, **rozhodně se podívejte na celé video z akce**:
<div class='embed-container'><iframe src='https://www.mall.tv/embed/maker-faire-prague/stratocaching-2020?autoplay=false' frameborder='0' allowfullscreen></iframe></div>

<br><!--So sorry for this hack here-->
Na sondě byla i kamera, která celý let zaznamenávala. Na **to nejzajímavější ze záznamu se můžete podívat tady**:
<div class='embed-container'><iframe src='https://www.mall.tv/embed/maker-faire-prague/stratocaching-sestrih-z-kamery-v-balonove-sonde?autoplay=false' frameborder='0' allowfullscreen></iframe></div>

## Prozkoumejte naše stránky!

Najdete zde třeba [celý výčet toho, co děláme](https://patek.cz/about/), [jak bojujeme proti COVIDu](https://patek.cz/blog/covidbreak-part2/) a [mnoho dalšího](https://patek.cz)!

## Nějaké otázky?

Pokud se chcete na něco zeptat, tak nás určitě [neváhejte kontaktovat](https://patek.cz/contacts/).
