---
title: "Join us!"
date: 2021-12-30T19:03:16+01:00
authors: [ "Greenscreener" ]
---
Like what you see? Do you want to join us or just find out what we really do? Come and visit us!

You don't have to be a student of the school, we have members from many different places and we'll be glad to welcome you too.

We meet every **friday afternoon**, except holidays and other circumstances that force us to stay home. Everybody arrives when they can, the first people come around noon and the last person departs usually around six. This is true for you too, so, come whenever you like.

What we do also isn't strictly determined, we're usually divided into groups and everybody works on something different. Talks also usually take place only after a request during a meeting, so, if there's anything that interests you, don't be afraid to ask us and maybe you'll earn an improvised talk by one of us.

We meet in the **physics lab**, but you don't have to search for us. When you arrive, let us know in our [Telegram](https://t.me/patekvpatek) and someone will come to open the door for you and show you around.

If you're considering visiting Pátek, let us know either on Telegram or via [e-mail](mailto:patek@gbl.cz). We can tell you how many of us plan to come, what are we going to do or if a meeting is happening that friday in the first place. It's also good for us to know in advance, so we can be ready for you.

We're looking forward to meeting you!
