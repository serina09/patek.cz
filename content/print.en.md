---
title: "Print"
date: 2019-09-08T12:45:28+02:00
authors: [ "Greenscreener" ]
buttonColor: "#ff7300"
buttonTextColor: "black"
menu:
  main:
    weight: 9
  landing:
    weight: 5
    title: "<patek-logo title='Print' subtitle='Pátek'>Print</patek-logo>"
---
Our grammar school has two Prusa RepRap i3 Mk3 3D printers, which are now managed by four members of Pátek. For that reason, we print almost constantly during our meetings and every member can, with the help of more experienced members, design and then print anything. Most people, however, choose to just download a shitty model from [thingiverse](https://thingiverse.com) and print that.

<a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSeBGasvDgj937T58yLhyG4WUVP7DMUAVAZYNjg5YkKqj1Ulaw/viewform">Ordering form</a>
