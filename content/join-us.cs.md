---
title: "Přidej se k nám!"
date: 2021-12-30T19:03:16+01:00
authors: [ "Greenscreener" ]
---
Zaujalo tě, co děláme? Chceš se k nám přidat nebo se alespoň podívat, jak to u
nás chodí? Neboj se na nás přijít podívat!

Nemusíš být student našeho gymnázia, máme mezi sebou i členy dojíždějící z blízka i z daleka a i tebe rádi přivítáme.

Když zrovna nejsou prázdniny nebo nás něco jiného nenutí k zůstání doma,
scházíme se **každý Pátek odpoledne**, ale každý přichází, jak má zrovna čas. První se trousí okolo poledne a poslední odchází okolo šesté večer, takže prostě přijď, jak budeš mít čas.

Program také není pevně daný, obvykle jsme rozdělení do skupinek a každý pracuje na něčem jiném. Přednášky také obvykle probíhají až na základě podnětu během schůze, takže pokud tě bude něco zajímat, neboj se zeptat a možná ti někdo poskytne i improvizovanou přednášku.

Scházíme se v **laborkách fyziky**, ale nemusíš se bát, že nás nenajdeš. Až dorazíš, napiš nám na [Telegram](https://t.me/patekvpatek) a někdo ti přijde otevřít, uvítá tě a do laborek tě dovede.

Pokud uvažuješ, že dorazíš, napiš nám buď na Telegram nebo na [e-mail](mailto:patek@gbl.cz) a my ti dáme vědět, kolik nás na daný Pátek dorazí, na co se můžeš přibližně těšit a jestli třeba není schůze zrušená. Také nám se hodí, když dáš vědět dopředu, abychom s tebou počítali.

Těšíme se na tebe!
