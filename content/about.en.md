---
title: "About us"
date: 2019-07-12T14:54:32+02:00
authors: [ "Greenscreener" ]
buttonColor: "#1CA620"
menu:
  main:
    weight: 19
  landing:
    weight: 1
---
While "pátek" is the name of the fifth day of the week in Czech, Pátek is a group of students interested not only in IT and programming but also physics, electrical engineering, amateur radio, math, biology and anything interesting in general. Like [the Friday Men](https://en.wikipedia.org/wiki/Friday_Men) met in Karel Čapek's house every Friday afternoon, we meet every Friday afternoon in one or more classrooms in the grammar school in Brandýs nad Labem after all the sane students have gone home. A big part of these meetings is spent with discussions about absurd topics (e.g. the theory of thought, anti-thought and their mutual annihilation), arguing about controversial topics (e.g. why Linux is unbeatably better than all other OSes) and people tend to work on stuff they procrastinated to the last moments. We also do some *useful* stuff:

### PátekTalks
In the spirit of a known Czech saying, that can be roughly translated to *"Who is good at something does it, who is bad at something teaches it."* students, who know something the others don't, prepare talks about topics they consider interesting. Because most members are conservative computer scientists, most these talks follow this direction, but if there is anyone brave enough, they can stand up to them with their topics.

### Projects
Members of Pátek are a creative bunch, we like to make stuff, bodge stuff and generally mess around. We learn new things and waste time that could be used for much more valuable activities. One of the reasons we had for creating this website was our desire to share our experiences with these projects with other members as well as with unsuspecting members of the public. If you try hard enough, you might even find some interesting information about ongoing and past projects.

### Trips
When there is a chance, we like to go on many trips, for instance to [100vědců](http://100vedcu.cz/).

### PátekPrint
Our grammar school has two Prusa RepRap i3 Mk3 3D printers, which are now managed by four members of Pátek. For that reason, we print almost constantly during our meetings and every member can, with the help of more experienced members, design and then print anything. Most people, however, choose to just download a shitty model from [thingiverse](https://thingiverse.com) and print that.

### Programming
As was mentioned before, a big part of our group are programmers, who spend their time staring at the displays of their computers, observing and writing colourful letters that don't make any sense. Besides heated debates about balderdash, they maintain [the grammar school's website](https://gbl.cz) and [this one]({{< relref "/" >}}). It doesn't end there though, one of the currently suspended projects is Gomber - an openSource implementation of BoberMan written in Go, with which we discover the basics of this beautiful programming language. We also help manage the school's network and computer infrastructure.

### PátekSpace
One could call our little group a [hackerspace](https://en.wikipedia.org/wiki/Hackerspace). One of the main advantages of a hackerspace is the availability of tools which would otherwise not be feasible to have at home. Currently, we have a few 3D printers, a CNC router for PCB milling and a stitching machine, and after prior arrangement, we sometimes borrow some of the lab kit that our school owns (obviously under the surveillance of a teacher 😉). We are still trying to gather more tools to make available to all members.

### Our partner groups
If you like Pátek, you will like these groups and clubs as well. They all share our values, are Pátek's friends and you're highly encouraged to check them out:

 - [Microlab](https://microlab.space/) - informal student hackerspace by the Department of Information Technology, Faculty of Education, CUNI

<br />
<br />
<br />

But most importantly, we are a bunch of friends doing what we enjoy. I hope that you find something useful on this website and finally, there's nothing else left than wishing you a good day.

Representing all members of Pátek,

[Greenscreener]({{< relref "/authors/greenscreener" >}})
