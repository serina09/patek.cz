---
title: "Páteční Sled Přednášek III (16. 10. 2020)"
date: 2020-10-14T20:27:00+02:00
authors: [ "Kyslík" ]
tags: [ "psp" ]
---

{{< button href="https://meet.jit.si/P%C3%A1tek" text="Připojit se k přednášce" >}}
<br>

[Livestream na YouTube](https://youtu.be/rEPahyUnng4)

Nazdar Pátečníci,

opět přecházíme na online formu Pátků a s ní opět přicházejí Páteční Sledy Přednášek. Tentokrát bude probíhat opravdu v **pátek** a to **16. 10. 2020**. Samotné přednášky budou opět probíhat na [Jitsi](https://meet.jit.si/Pátek) a budeme je streamovat na [YouTube](https://youtu.be/rEPahyUnng4), kde bude následně i záznam.

Co však není obvyklé je, že dva přednášející, konkrétně **Petr Velička** a **Emil Miler**, jsou hosté z [microlabu](https://microlab.space/), což je podobný spolek Pátku na [PedF UK](https://pedf.cuni.cz/PEDF-1.html). Tímto jim moc děkujeme.

Nyní však již k programu:

***Přednáška Petra a Emila je odložena na příští, respektive popříští týden a dnešní program je obstarán Honzou Černohorským a Vojtou.***

15:00 - **Vojta Káně** - [Pohled pod pokličku patek.cz]({{< relref "/talks/patek_cz-internals" >}})

15:30 - **Petr Velička** - [lfs-musl-clang]({{< relref "/talks/lfs" >}})

16:00 - **Emil Miler (Nixi)** - [>install gentoo]({{< relref "/talks/install-gentoo" >}}) (pokud stihne dorazit)

Pokud jste tak ještě neučili a chcete, máte možnost se přihlásit k odběru novinek o těchto akcích [zde](https://forms.gle/SJpUP9XvicqN8ZBo6).
