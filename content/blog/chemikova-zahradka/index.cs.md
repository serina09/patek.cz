---
title: "Chemikova Zahrádka"
date: 2021-09-14T19:41:37+02:00
authors: [ "Saša" ]
tags: ["chemie","mfp2021"]
---

Chemikova zahrádka je efektní a jednoduchý pokus, který se shledal se značným úspěchem
na Dni otevřených dveří, Letním festivalu GBL a ve spojení s LED páskem a kapacitním
senzorem dotyku také na Maker Faire Prague 2021.

{{< blogPhoto "photo1.jpeg" "Detail nasvíceného akvária" >}}

{{< blogPhoto "photo2.jpeg" "Nenasvícené akvárium" >}}

Příprava experimentu je značně jednoduchá a bezpečná, i přesto doporučuji mít nasazené
ochranné brýle a rukavice. Jak tedy experiment připravit a provést?

1. Najít vhodnou průhlednou nádobu, ve které budeme pokus provádět.
2. Zředit vodní sklo (roztok Na 2 SiO₃ , volně dostupné v drogeriích) vodou v poměru 1:1,
ideálně ve vybrané nádobě z kroku č. 1. (Je důležité směs dobře promíchat,
poznáme to tak, že vzniklá směs je jednolitá)
3. Do směsi vhodíme vhodné soli kovů. (seznam níže) Vhodnější než jemný prášek
jsou větší kousky.
4. Počkat než vyrostou nádherné útvary podobné rostlinám či korálům.

Vhodné soli a příslušné barvy útvarů:
- CuSO₄ = světle modrá, v drogerii volně dostupné jako modrá skalice
- FeSO₄ = tmavě zeleno-šedá, v drogerii volně dostupné jako zelená skalice
- FeCl₃ = rezavá, dostupné jako roztok pro leptání PCB, nutné odpařit v troubě
- KAl(SO₄)₂ = bílá, dostupné v drogerii jako kamenec
- CoSO₄ = tmavě modrá
- NiCl₂ = světle zelená, dostupné na internetu
- MgSO₄ = bílá, dostupné v drogerii jako hořká sůl
- CrCl₃ = tmavě zelená
- MnSO₄ = světle růžová
- CaCl₂ = bílá, dostupné na internetu, alternativně lze použít Ca(NO₃)₂, dostupné v
drogerii jako ledek vápenatý.

Na internetu je dostupná celá sada na tento pokus, koupit ji bude pravděpodobně levnější,
než shánění velkého přebytku solí, pro které nemusíte mít využití následné využití.
