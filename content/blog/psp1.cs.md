---
title: "První Páteční Sled Přenášek"
date: 2020-05-14T21:37:00+02:00
authors: [ "Greenscreener" ]
tags: [ "psp" ]
---

[Záznam livestreamu na YouTube](https://www.youtube.com/watch?v=wkN9iwk0dSQ)

Ahoj všichni Pátečníci, příznivci Pátku, náhodní kolemjdoucí nebo (daleko pravděpodobněji) lidé, kterým jsme tento odkaz nacpali násilím.

Inspirováni například KSPčkovými Krátkými Streamovanými Přednáškami jsme se rozhodli v rámci Pátku a PátekTalks zorganizovat vlastní takovou akci.

První Páteční Sled Přednášek se bude konat **v pondělí, 18. 5. 2020 od 14:00**. O programu zatím hlasujeme na [Pátečním Telegramu](https://t.me/patekvpatek), až bude odhlasováno a naplánováno, program se objeví také zde. Přednášky by měly trvat **60 - 90 min**, po nich následuje volné diskusní odpoledne, kdo bude mít čas, naskytne se mu příležitost pořádně si Pátečnicky pokecat.

Potkáme se ~~jako obvykle na [Jitsi](https://meet.vpsfree.cz/Pátek)~~ **AKTUALIZOVÁNO 18. 5.: Setkáme se přímo na serveru Jitsi místo VPSFree [zde](https://meet.jit.si/P%C3%A1tek)**. Pokud nám to technika dovolí, pokusíme se zhotovit záznam, který zveřejníme zde.

Pokud chcete dostávat informace o nadcházejících přednáškách emailem, přihlašte se [zde](https://forms.gle/SJpUP9XvicqN8ZBo6). Kromě těchto adres budeme také rozesílat informace do obvyklých komunikačních kanálů Pátku.

**Aktualizováno 16. 5. - Program:**

14:00 - **Saša Fišera** - Termit a směsi jemu podobné ([záznam](https://www.youtube.com/watch?v=_aeF4AqAjqQ))

~14:30 - **Franta Kmječ** - Rychlost programů (základy programování, díl první) ([záznam](https://www.youtube.com/watch?v=qjoYp42F08k))

~15:00 - **Šimon Šustek** - OpenPGP: každý je certifikační autorita ([záznam](https://www.youtube.com/watch?v=TSiyKBtUSLI))

~15:30 - začátek dotazů a volného tlachání

