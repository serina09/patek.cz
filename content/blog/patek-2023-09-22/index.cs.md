---
title: "Pátek 22. 9. 2023 – Prezentace o e-mailech a frézování"
date: 2023-10-10T21:35:04+0200
authors: [ "Glassknight" ]
tags: [ "schuze" ]
---

Dnes byla velmi vysoká účast, bylo tu asi 30 členů. Asi protože tento pátek byl
velmi nabitý. Začal [přednáškou o emailech]({{< relref "/talks/email-1" >}}),
kde [Vojta Káně]({{< relref "/authors/vojta001" >}}) zašel do historického
kontextu, struktuře emailu a jak se email přeposílá. Zmínil možnosti v hlavičce,
a různé kuriozity, což bylo zajímavý i pro ty, kteří o emailu už mnoho ví.

Po přednášce jsem začal frézovat do bronzového pečetítka. Frézku na pátku už
máme přes rok, ale poprvé frézujeme tvrdší materiál. První pokusy byly na
cuprexitu, když se chystala dejvutova meteostanice na makerfaire Praha. Před
týdnem jsem zkoušel do dřeva, a dnes do bronzu. Nakonec vše šlo hladce, vrták
(v-bit) vypadá ostře, protože jsem ho ručně chladil. Už mi stačí jen sehnat
vosk, abych mohl pečetit obálky.


{{< blogPhoto src="rezani.jpeg" alt="Frézka frézující pečetítko" >}}

{{< blogPhoto src="pecet.jpeg" alt="Hotové pečetítko s pečetí" >}}
