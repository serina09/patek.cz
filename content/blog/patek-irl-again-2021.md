---
title: "Pátek opět prezenčně"
date: 2021-05-24T11:13:47+02:00
authors: [ "vojta001" ]
tags: [ ]
---

Neuvěřitelné se stalo skutečností a po mnohaměsíční pauze proběhl v <time datetime="2021-05-21-">pátek 21. 5.</time> Pátek v našem milovaném videosálku. Ano, po vstupu je třeba absolvovat test, nebo mít potvrzení o testu z testovacího centra a uvnitř budovy je nutné mít nasazený respirátor, ale přesto **můžeme se vidět**, **křičet jeden přes druhého** a **velmi rázně gestikulovat**. Ne, že by JitSi neodvádělo dobrou práci, ale osobní setkání zatím nenahradí.

## Co teď?

**Přijďte**! Pátek online sice probíhal prakticky bez výpadků, ale účast postupně upadala a vydrželi jen nejstarší členové. Celkem tomu rozumím, náplň online setkání byla dosti svérázná, ale tím spíš teď rád potkám úplně všechny.

**Tvořte**! 3D tiskárny jsou připraveny, Pátek meteostanice už je živoucí legendou a novinky posledních dní nahrávají i zájemncům o síťování či administraci. A talk už taky dlouho nebyl…
