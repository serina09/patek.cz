---
title: "Kontakt"
menu:
  main:
    weight: 20
---

Nejlepší způsob, jak nás kontaktovat je naše Telegramová skupina:
[t.me/patekvpatek](https://t.me/patekvpatek).

Také máme emailovou skupinu: [patek@gbl.cz](mailto:patek@gbl.cz)

Pátek má na starosti [Ing. Eva Kospachová](mailto:e.kospachova@gbl.cz) a vede ho
společně s [Vojtou Kánětem](mailto:vojtech.kane@gbl.cz), [Janem
Černohorským](mailto:jan.cernohorsky@gbl.cz), [Tomášem
Kyselou](mailto:tomas.kysela@gbl.cz) a [Šimonem
Šustkem](mailto:simon.sustek@gbl.cz).
