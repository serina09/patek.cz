---
title: "Space"
date: 2019-09-08T12:54:30+02:00
authors: [ "Greenscreener" ]
buttonColor: "#33007f"
menu:
  main:
    weight: 10
  landing:
    weight: 6
    title: "<patek-logo title='Space' subtitle='Pátek'>Space</patek-logo>"
---
One could call our little group a
[hackerspace](https://en.wikipedia.org/wiki/Hackerspace). One of the main
advantages of a hackerspace is the availability of tools which would otherwise
not be feasible to have at home. Currently, we have a few 3D printers, a CNC router for PCB milling and a stitching machine, and after prior arrangement, we sometimes borrow some of the lab kit that our school owns (obviously under the surveillance of a teacher 😉). We are still trying to gather more tools to make available to all members.
