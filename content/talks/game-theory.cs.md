---
title: "Teorie her"
date: 2020-05-25T18:30:19+02:00
authors: [ "Lenka" ]
when: 2020-05-25T15:00:00+02:00
slides:
  - data: https://www.overleaf.com/read/hrzhqscrbrjf
recordings:
  - data: https://www.youtube.com/watch?v=DdY-cXTfw9s
---
Pověděli jsme si něco o teorii her, což je prostě oblast matiky, která se zabývá (překvapivě) hrami a různými strategiemi. Ty se se snaží nějak porovnávat podle toho, jak jsou přínosné. Na začátku jsme si jednu takovou velmi velmi jednoduchou hru zahráli, následně jsme si pověděli něco o ryzích a smíšených strategiích. Taky jsme probrali, co je to Nashovo ekvilibrium a ukázali si ho na pár příkladech - např. známé hry typu vězňovo dilema nebo bitva pohlaví.
