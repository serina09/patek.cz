---
title: "Termit a směsi jemu podobné"
date: 2020-05-21T11:47:19+02:00
authors: [ "Saša" ]
when: 2020-05-18T14:00:00+02:00
slides:
  - data: https://docs.google.com/presentation/d/1n9N2aTcueDRrwlLDfBMyL-XJUKrppYX9BiR2eRaMaIA
recordings:
  - data: https://www.youtube.com/watch?v=_aeF4AqAjqQ
---
Se Sašou jsme zjistili, co jako termit vlastně označujeme, jak jej vyrobit i k čemu to je nakonec dobré. Nechybělo ani poučení o bezpečnosti a demonstrace jednoho termitu naživo.
