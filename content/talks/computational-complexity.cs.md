---
title: "Rychlost programů: úvod do asymptotické složitosti"
date: 2020-05-19T14:53:45+02:00
authors: [ "Franta" ]
when: 2020-05-18T14:30:00+02:00
slides:
  - data: https://docs.google.com/presentation/d/1zJK37UjmgbKOJKhY0ZsHLlB5nX7MemSf-7O20cQI7GY
recordings:
  - data: https://www.youtube.com/watch?v=qjoYp42F08k
---
Půlhodinový neformální úvod do toho, jak přemýšlet nejen nad rychlostí programů, ale hlavně teoretických algoritmů.
