---
title: "Devops and SRE"
date: 2022-02-16T21:48:42+01:00
authors: [ "epcim" ]
when: 2022-02-18T15:30:00+01:00
slides:
  - data: https://slides.com/petrmichalec/patek-devops
recordings:
---
Ohlednutí a kam směřují trendy vývoje, automatizace a provozu infrastruktury.
