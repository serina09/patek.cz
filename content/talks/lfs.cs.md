---
title: "Linux From Scratch"
date: 2020-04-15
authors: [ "petrvel" ]
when: 2020-10-16T17:00:00+02:00
---

Linux From Scratch -- říká Vám něco název této knihy?

Pokud ne, na této přednášce budete mít příležitost poznat svět Linuxu ještě více do podrobna a dozvíte se o tom, jak si sestavit vlastní Linuxovou distibuci doslova od začátku, tzn. nezakládající se na žádné jiné existující distribuci. Můžete se těšit na hodně kompilování, ale nesmíte zapomenout ani na čtení dokumentace, jejíž rozsah několikrát přesahuje rozsah Vojny a míru.
