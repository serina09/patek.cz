---
title: "Hesla"
date: 2022-01-03T18:53:55+01:00
authors: [ "Greenscreener", "Sijisu" ]
when: 2022-01-05T7:55:00+01:00
slides:
  - data: /talks/passwords/slides
---
S hesly se potkáváme každý den, ale málokdo jim věnuje takovou pozornost, kterou by si zasloužily. Proč je *Pepicek84!* špatné heslo? Jak vytvořit dobré heslo? Jak jinak chránit své účty na internetu? Na tohle vše a mnoho dalšího se vám pokusíme odpovědět v naší prezentaci, která vznikala přes rok a konečně jsme se dokopali k jejímu odprezentování.
