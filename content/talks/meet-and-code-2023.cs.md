---
title: "Meet and Code 2023"
date: 2023-10-12T12:29:23+02:00
authors: [ "avekos", "Greenscreener", "vojta001", "kyslík", "Dejwut", "sijisu" ]
when: 2023-10-13T14:00:00+02:00
---
Hlavní náplní workshopu je naprogramování známé hry Had. Účastníci si vyberou
mezi třemi možnostmi, kde tvořit: Scratch, Python nebo vytvoření webové
aplikace. Různorodé programy umožňují pracovat i naprostým začátečníkům, takže
se není čeho bát.

Druhou možnou aktivitou bude sestavení libovolného robota ze stavebnice VEX.
Sada obsahuje několik motorů a různých senzorů. Výzvou je naprogramování
ovládání robota pomocí dálkového ovladače a projet nejrychleji připravenou
dráhu.

Ukážeme si, jak snadno lze sestavit program s využitím připravených bloků.
Zvládneme si nastavit grafické prostředí, ovládat pohyb, počítat skóre,
vyhodnotit hru...

Společně otevřeme dveře a uděláme první krok do světa programování. Ukážeme si,
jak je jednoduché vytvořit si vlastní hru nebo ovládat robota. Účastníci se
seznámí se základy programování v různých prostředích, vyzkouší si manuální
sestavení robota a jeho chování.

Akce je určena pro všechny zájemce, začátečníky i pokročilé. Vyzýváme hlavně
všechny slečny, určitě se nebojte přijít. :)

Účastníkům se budou věnovat 4 lektoři s pomocníky z řad zkušených žáků a žákyň.

Jednotlivé aktivity budou probíhat mezi 14.00 a 18.00 hod na čtyřech
stanovištích a poběží paralelně, tj. programování v jednotlivých jazycích a
roboty. Účastníci mohou přijít ve slotech, kdy začne workshop, hodinové
workshopy (14:00, 15:00, 16:00 a 17:00) budou zacílené na cílovou skupinu dětí a
žáků. Mladší účastníci nejspíš dají přednost programování ve Scratchi nebo
robotům, starší účastníci možná sáhnou po Pythonu, webové aplikaci nebo
robotech.

[Web Meet and Code](https://meet-and-code.org/cz/cs/event-show/11169)

