---
title: "Pohled pod pokličku patek.cz"
date: 2020-10-14T17:21:50+02:00
authors: [ "vojta001" ]
when: 2020-10-16T15:00:00+02:00
---

[patek.cz]({{< relref "/" >}}) je klasická webovka obsahující základní informace o svém provozovateli a blog. Nebo ne?

Statické generování stránek (SSG) nabírá na popularitě, přesto se dnes jedná spíše o raritu. Jak to děláme my a co nám to přináší? A hodí se to na vše?

Zájemcům, kteří výše uvedené již znají, představím projekt [Hugo](https://gohugo.io) a ukážu, co a kde se v na první pohled zmatené složkové struktuře skrývá. Také vysvětlím jazyk [HTML šablon](https://golang.org/pkg/html/template) z jazyka [Go](https://golang.org), který Hugo použivá.
