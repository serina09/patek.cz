---
title: "Talks"
date: 2019-08-06T09:42:55+02:00
authors: [ "Greenscreener" ]
buttonColor: "red"
navColor: "red"
menu:
  main:
    weight: 8
  landing:
    weight: 4
    title: "<patek-logo title='Talks' subtitle='Pátek'>Talks</patek-logo>"
cascade:
  outputs:
    - HTML
    - RSS
    - Calendar
---
Dle známého českého přísloví *„Kdo umí, umí, kdo neumí, učí.“* si členové, kteří vědí něco, co ostatní neví, připravují jednou za čas přednášky o tématech, která jim připadají zajímavá. Vzhledem k tomu, že většina starších členů Pátku jsou skalní informatici, se většina přednášek ubírá tímto směrem, avšak kdo má odvahu, může se jim postavit se svým tématem.

Samozřejmě budeme moc rádi, pokud se k nám s přednášením přidá i někdo zajímavý, kdo není členem našeho uskupení. 

Na této stránce budeme zveřejňovat data plánovaných přednášek, jejich krátké popisy a následně také slidy, pokud to přednášející umožní.
