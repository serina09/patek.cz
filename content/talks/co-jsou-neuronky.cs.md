---
title: "Co jsou neuronky; základní myšlenky strojového učení"
date: 2022-06-16T12:15:55+02:00
authors: [ "Franta" ]
when: 2022-06-17T16:00:00+02:00
---
Strojové učení hýbe světem a řeší problémy, o kterých jsme si pomalu ani
nemysleli, že jsou rozumně řešitelné. V přednášce/workshopu projdeme pár
základních principů, které rozhodně potkáte, když budete chtít cokoliv v ML
dělat, a vše si ukážeme na praktické ukázce. Cílem je trochu si to vyzkoušet a
znát základní pojmy, aby vás pak tolik nepřekvapovala dokumentace, když budete
potřebovat něco naprogramovat/natrénovat. Těšit se můžete na trochu matiky a
snad docela hodně intuice.  Hodí se vědět, co to je derivace, ale není to
podmínkou.
