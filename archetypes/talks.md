---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
authors: [ "unknown" ]
when: {{ .Date }}
slides:
  - data: <url>
recordings:
  - data: <url>
---
